package composer;

import shapes.AbstractShape;
import shapes.ComposedShape;

public class ComposedComposer extends AbstractComposer {

	@Override
	public AbstractShape create(int x, int y) {
		shape = new ComposedShape();
		shape.setStart(x, y);
		return shape;
	}

	@Override
	public AbstractShape expand(int x, int y) {
		return this.complete(x, y);
	}

	@Override
	public AbstractShape complete(int x, int y) {
		shape.setEnd(x, y);
		return shape;
	}

	@Override
	public AbstractShape getShape() {
		return shape;
	}

}
