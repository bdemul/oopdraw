package composer;

import shapes.AbstractShape;

public class OvalComposer extends AbstractComposer {

	@Override
	public AbstractShape create(int x, int y) {
		shape = new shapes.Oval();
		shape.setStart(x, y);
		return shape;

	}

	@Override
	public AbstractShape expand(int x, int y) {
		this.complete(x, y);
		return shape;

	}

	@Override
	public AbstractShape complete(int x, int y) {
		shape.setEnd(x, y);
		;
		return shape;

	}

	@Override
	public AbstractShape getShape() {
		return this.shape;
	}

}
