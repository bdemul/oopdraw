package composer;

import shapes.AbstractShape;

public class LineComposer extends AbstractComposer {

	@Override
	public AbstractShape create(int x, int y) {
		shape = new shapes.Line();
		shape.setStart(x, y);
		return shape;

	}

	@Override
	public AbstractShape expand(int x, int y) {
		this.complete(x, y);
		return shape;

	}

	@Override
	public AbstractShape complete(int x, int y) {
		shape.setEnd(x, y);
		return shape;

	}

	@Override
	public AbstractShape getShape() {
		return this.shape;
	}

}
