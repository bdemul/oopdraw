package composer;

import shapes.AbstractShape;
import shapes.Arc;

public class ArcComposer extends AbstractComposer {

	public AbstractShape create(int x, int y) {
		shape = new Arc();
		shape.setStart(x, y);
		return shape;

	}

	@Override
	public AbstractShape expand(int x, int y) {
		this.complete(x, y);
		return shape;

	}

	@Override
	public AbstractShape complete(int x, int y) {
		shape.setEnd(x, y);
		return shape;

	}

	@Override
	public AbstractShape getShape() {
		return this.shape;
	}
}
