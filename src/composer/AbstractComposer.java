package composer;

import shapes.AbstractShape;

public abstract class AbstractComposer {
	
	protected AbstractShape shape;
	

	public abstract AbstractShape create(int x, int y);

	public abstract AbstractShape expand(int x, int y);

	public abstract AbstractShape complete(int x, int y);

	public abstract AbstractShape getShape();
}
