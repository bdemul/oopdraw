package view;

import java.awt.Button;
import java.awt.FlowLayout;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JPanel;

import model.ButtonLintModel;
import model.CanvasModel;
import controller.ButtonLintController;
import controller.ClearController;

public class ButtonLint extends JPanel implements Observer {

	private static final long serialVersionUID = -96693076346321962L;
	private ButtonLintModel lintModel;
	private ButtonLintController buttonLintController;
	private ClearController clearController;

	public ButtonLint(ButtonLintModel lintModel, CanvasModel canvasModel) {
		this.setSize(1024, 150);
		this.lintModel = lintModel;
		// set layout
		this.setLayout(new FlowLayout());
		// set the observers
		lintModel.addObserver(this);
		//init the model
		lintModel.init();
		
		// create the buttonLintController
		this.buttonLintController = new ButtonLintController(lintModel, this);
		this.clearController = new ClearController(canvasModel);
		
		
		this.update(lintModel, "");

	}

	@Override
	public void update(Observable o, Object arg) {
		this.removeAll();
		// create button
		for (String name : this.lintModel.listCompserNames()) {
			Button button = new Button(name);
			button.addActionListener(this.buttonLintController);
			this.add(button);
		}
		// create button clear
		Button btnClear = new Button("Clear");
		btnClear.addActionListener(clearController);
		// add everything
		this.add(btnClear);
	}
}
