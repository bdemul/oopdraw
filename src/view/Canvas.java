package view;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JPanel;

import model.ButtonLintModel;
import model.CanvasModel;
import shapes.AbstractShape;
import controller.CanvasController;

public class Canvas extends JPanel implements Observer{
	
	private CanvasModel model;
	private CanvasController controller;
	
	public Canvas(CanvasModel canvasModel, ButtonLintModel lintModel){
		this.setSize(1024, 606);
		this.model = canvasModel;
		this.controller = new CanvasController(canvasModel, this, lintModel);
		canvasModel.addObserver(this);
		this.addMouseListener(controller);
		this.addMouseMotionListener(controller);
	}

	
	@Override
	public void paint(Graphics g) {
		BufferedImage bufferedImage = new BufferedImage(getSize().width, getSize().height, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2d = bufferedImage.createGraphics();
		RenderingHints hints = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2d.setRenderingHints(hints);
		g2d.setColor(Color.black);
		g2d.fillRect(0, 0, getSize().width, getSize().height);
		g2d.setColor(new Color(255, 255, 154));
		g2d.fillRect(1, 1, getSize().width - 3, getSize().height - 3);
		ArrayList<AbstractShape> shapes = this.model.getShapes();
		for (int i = 0; i < shapes.size(); i++) {
			shapes.get(i).draw(g2d);
		}
		Graphics2D g2dComponent = (Graphics2D) g;
		g2dComponent.drawImage(bufferedImage, null, 0, 0);
	}


	@Override
	public void update(Observable o, Object arg) {
		repaint();
		
	}
}
