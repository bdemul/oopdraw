package view;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JSplitPane;
import javax.swing.WindowConstants;

import model.ButtonLintModel;
import model.CanvasModel;

public class OOPDraw extends JFrame {
	public OOPDraw() {
		this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		this.setSize(1024, 756);
		JSplitPane splitPane = new JSplitPane();
		splitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
		getContentPane().add(splitPane, BorderLayout.CENTER);
		
		ButtonLintModel lintModel = new ButtonLintModel();
		CanvasModel canvasModel = new CanvasModel();
		ButtonLint lint = new ButtonLint(lintModel, canvasModel);

		splitPane.setTopComponent(lint);
		
	
		Canvas canvas = new Canvas(canvasModel, lintModel);
		
		
		splitPane.setBottomComponent(canvas);
	}
	
	public static void main(String[] args){
		OOPDraw frame = new OOPDraw();
		frame.setVisible(true);
	}

	
}
