package controller;

import java.awt.Button;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

import model.ButtonLintModel;
import view.ButtonLint;

public class ButtonLintController implements ActionListener, Observer {
	
	private ButtonLintModel lintModel;
	private ButtonLint buttonLint;


	public ButtonLintController(ButtonLintModel lintModel, ButtonLint buttonLint) {
		this.lintModel = lintModel;
		this.buttonLint = buttonLint;
		lintModel.addObserver(this);
	}

	@Override
	public void actionPerformed(ActionEvent evt) {
		lintModel.createComposer(((Button)evt.getSource()).getLabel());

	}

	@Override
	public void update(Observable arg0, Object arg1) {
		// TODO Auto-generated method stub
		
	}

}
