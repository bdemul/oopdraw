package controller;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.Observable;
import java.util.Observer;

import model.ButtonLintModel;
import model.CanvasModel;
import view.Canvas;

public class CanvasController implements Observer, MouseListener,
		MouseMotionListener {

	private CanvasModel canvasModel;
	private Canvas canvasView;
	private ButtonLintModel lintModel;

	public CanvasController(CanvasModel canvasModel, Canvas canvasView,
			ButtonLintModel lintModel) {
		this.canvasModel = canvasModel;
		this.canvasView = canvasView;
		this.lintModel = lintModel;
		lintModel.addObserver(this);
		canvasModel.addObserver(this);
	}

	@Override
	public void update(Observable o, Object arg) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseDragged(MouseEvent e) {
		int x = e.getX();
		int y = e.getY();
		this.lintModel.getCurrentComposer().expand(x, y);
		this.canvasView.repaint();

	}

	@Override
	public void mouseMoved(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent e) {
		int x = e.getX();
		int y = e.getY();
		this.canvasModel.setShape(this.lintModel.getCurrentComposer().create(x,y));
		this.canvasView.repaint();

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		int x = e.getX();
		int y = e.getY();
		this.lintModel.getCurrentComposer().complete(x, y);
		this.canvasView.repaint();

	}

}
