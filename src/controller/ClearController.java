package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

import model.CanvasModel;

public class ClearController implements Observer, ActionListener {
	
	private CanvasModel canvasModel;

	
	public ClearController(CanvasModel canvasModel){
		this.canvasModel = canvasModel;

	}
	
	

	@Override
	public void update(Observable arg0, Object arg1) {
		// TODO Auto-generated method stub

	}



	@Override
	public void actionPerformed(ActionEvent e) {
		canvasModel.clear();
		
	}

}
