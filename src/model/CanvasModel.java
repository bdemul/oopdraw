package model;

import java.util.ArrayList;
import java.util.Observable;

import shapes.AbstractShape;

public class CanvasModel extends Observable{
	
	ArrayList<AbstractShape> shapes;
	
	public CanvasModel(){
		shapes = new ArrayList<AbstractShape>();
	}

	public ArrayList<AbstractShape> getShapes() {
		return shapes;
	}
	
	public void setShape(AbstractShape shape){
		shapes.add(shape);
		this.setChanged();
		this.notifyObservers();
	}

	public void clear() {
		shapes.clear();
		this.setChanged();
		this.notifyObservers();
		
	}

}
