package model;

import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import java.util.Set;

import composer.AbstractComposer;
import composer.ArcComposer;
import composer.ComposedComposer;
import composer.LineComposer;
import composer.OvalComposer;
import composer.RectComposer;
import composer.SmileyComposer;

public class ButtonLintModel extends Observable {

	private Map<String, AbstractComposer> map;
	private AbstractComposer currentComposer;

	public ButtonLintModel() {
		
	}
	
	public void init(){
		map = new HashMap<String, AbstractComposer>();
		map.put("line", new LineComposer());
		map.put("rect", new RectComposer());
		map.put("oval", new OvalComposer());
		map.put("funny", new ComposedComposer());
		map.put("Smiley", new SmileyComposer());
		map.put("arc", new ArcComposer());
		currentComposer = map.get("line");
		this.setChanged();
		this.notifyObservers();
	}

	public void createComposer(String key) {
		if (map.containsKey(key))
			currentComposer = map.get(key);
	}

	public Set<String> listCompserNames() {
		return map.keySet();
	}
	
	public AbstractComposer getCurrentComposer(){
		return this.currentComposer;
	}
}
