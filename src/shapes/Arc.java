package shapes;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.Arc2D;
import java.awt.geom.Rectangle2D;

public class Arc implements AbstractShape {
	
	private Point startPoint;;
	private int startAngle;
	private int angleExtent;
	private Rectangle2D rect;
	private Arc2D arc;
	private int closure;

	public Arc() {
		arc = new Arc2D.Double();
		rect = new Rectangle2D.Float();
		startPoint = new Point();
		startAngle = 0;
		angleExtent = -180;
		closure = Arc2D.OPEN;
	}

	@Override
	public void setStart(int x, int y) {
		startPoint.setLocation(x, y);
		rect.setFrame(x, y, 0, 0);
		arc.setArc(rect, startAngle, angleExtent, closure);

	}

	@Override
	public void setEnd(int x, int y) {
		rect.setFrameFromDiagonal(startPoint, new Point(x, y));
		arc.setArc(rect, startAngle, angleExtent, closure);

	}

	@Override
	public void draw(Graphics2D g) {
		g.setColor(Color.BLUE);
		g.draw(this.arc);

	}
	
	public void setClosure(int closure){
		this.closure = closure;
	}

	@Override
	public double getWidth() {
		return arc.getWidth();
	}

	@Override
	public double getHeight() {
		return arc.getHeight();
	}

}
