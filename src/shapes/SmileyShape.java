package shapes;

import java.awt.Point;
import java.awt.geom.Arc2D;
import java.util.ArrayList;

public class SmileyShape extends ComposedShape {
	
	private Point startPoint;
	private int signX;
	private int signY;
	
	public SmileyShape(){
		shapes = new ArrayList<AbstractShape>();
		shapes.add( new Arc());
		shapes.add(new Oval());
		shapes.add(new Oval());
		shapes.add(new Oval());
		((Arc)shapes.get(0)).setClosure(Arc2D.CHORD);
	}
	
	public void setStart(int x, int y){
		startPoint = new Point(x,y);
		shapes.get(1).setStart(x, y);
		setMouth();
		setEye1();
		setEye2();
	}
	
	public void setEnd(int x, int y){
		signX = x < startPoint.x ? -1 : 1;
		signY = y < startPoint.y ? -1 : 1;
		shapes.get(1).setEnd(x, y);
		setMouth();
		setEye1();
		setEye2();
	}
	
	private void setMouth(){
		int xStart = (int)Math.round(startPoint.x + shapes.get(1).getWidth() * signX * 0.25);
		int yStart = (int)Math.round(startPoint.y + shapes.get(1).getHeight() * (signY == -1 ? -0.25 : 0.75));
		shapes.get(0).setStart(xStart, yStart);
		int xEnd = (int)Math.round(startPoint.x + shapes.get(1).getWidth() * signX * 0.75);
		int yEnd = (int)Math.round(startPoint.y + shapes.get(1).getHeight()  *  (signY == -1 ? -0.50 : 0.50));
		shapes.get(0).setEnd(xEnd, yEnd);
	}
	
	private void setEye1(){
		int xStart = (int)Math.round(startPoint.x + shapes.get(1).getWidth() * signX *  0.15);
		int yStart = (int)Math.round(startPoint.y + shapes.get(1).getHeight() * (signY == -1 ? -0.75 : 0.25));
		shapes.get(2).setStart(xStart, yStart);
		int xEnd = (int)Math.round(startPoint.x + shapes.get(1).getWidth() * signX * 0.35);
		int yEnd = (int)Math.round(startPoint.y + shapes.get(1).getHeight() * (signY == -1 ? -0.55 : 0.45));
		shapes.get(2).setEnd(xEnd, yEnd);
	}
	
	private void setEye2(){
		int xStart = (int)Math.round(startPoint.x + shapes.get(1).getWidth() * signX * 0.65);
		int yStart = (int)Math.round(startPoint.y + shapes.get(1).getHeight() * (signY == -1 ? -0.75 : 0.25));
		shapes.get(3).setStart(xStart, yStart);
		int xEnd = (int)Math.round(startPoint.x + shapes.get(1).getWidth() * signX * 0.85);
		int yEnd = (int)Math.round(startPoint.y + shapes.get(1).getHeight() * (signY == -1 ? -0.55 : 0.45));
		shapes.get(3).setEnd(xEnd, yEnd);
	}
}
