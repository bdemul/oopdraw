package shapes;

import java.awt.Graphics2D;
import java.util.ArrayList;

public class ComposedShape implements AbstractShape {
	
	protected ArrayList<AbstractShape> shapes;
	
	public ComposedShape(){
		shapes = new ArrayList<AbstractShape>();
		shapes.add(new Line());
		shapes.add(new Rect());
		shapes.add(new Oval());
	}

	@Override
	public void setStart(int x, int y) {
		for (AbstractShape abstractShape : shapes) {
			abstractShape.setStart(x, y);
		}

	}

	@Override
	public void setEnd(int x, int y) {
		for (AbstractShape abstractShape : shapes) {
			abstractShape.setEnd(x, y);
		}

	}

	@Override
	public void draw(Graphics2D g) {
		for (AbstractShape abstractShape : shapes) {
			abstractShape.draw(g);
		}

	}

	@Override
	public double getWidth() {
		return 0;
	}

	@Override
	public double getHeight() {
		return 0;
	}

}
