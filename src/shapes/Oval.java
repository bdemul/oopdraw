package shapes;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.Ellipse2D;

public class Oval implements AbstractShape {

	private Point startPoint;;
	private Ellipse2D oval;

	public Oval() {
		oval = new Ellipse2D.Float();
		startPoint = new Point();

	}

	@Override
	public void setStart(int x, int y) {
		oval.setFrame(x, y, 0, 0);
		startPoint.setLocation(x, y);

	}

	@Override
	public void setEnd(int x, int y) {
		oval.setFrameFromDiagonal(startPoint, new Point(x, y));

	}

	@Override
	public void draw(Graphics2D g) {
		g.setColor(Color.RED);
		g.draw(this.oval);

	}

	@Override
	public double getWidth() {
		return oval.getWidth();
	}

	@Override
	public double getHeight() {
		return oval.getHeight();
	}

}
