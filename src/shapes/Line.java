package shapes;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.Line2D;

public class Line implements AbstractShape {

	private Point startPoint;;
	private Line2D line;

	public Line() {
		line = new Line2D.Float();
		startPoint = new Point();
	}

	@Override
	public void draw(Graphics2D g) {
		g.setColor(Color.BLACK);
		g.draw(this.line);

	}

	@Override
	public void setStart(int x, int y) {
		line.setLine(new Point(x, y), new Point(x, y));
		startPoint.setLocation(x, y);

	}

	@Override
	public void setEnd(int x, int y) {
		line.setLine(startPoint, new Point(x, y));

	}

	@Override
	public double getWidth() {
		return 0;
	}

	@Override
	public double getHeight() {
		return 0;
	}

}
