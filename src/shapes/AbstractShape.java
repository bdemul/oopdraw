package shapes;

import java.awt.Graphics2D;


public interface AbstractShape{
	
	public abstract void setStart(int x, int y);
	
	public abstract void setEnd(int x, int y);
	
	public abstract void draw(Graphics2D g);
	
	public abstract double getWidth();
	
	public abstract double getHeight();

}
