package shapes;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.Rectangle2D;

public class Rect implements AbstractShape {

	private Rectangle2D rect;
	private Point startPoint;

	public Rect() {
		rect = new Rectangle2D.Float();
		startPoint = new Point();
	}

	@Override
	public void setStart(int x, int y) {
		startPoint.setLocation(x, y);
		rect.setFrame(x, y, 0, 0);

	}

	@Override
	public void setEnd(int x, int y) {
		rect.setFrameFromDiagonal(startPoint, new Point(x, y));

	}

	@Override
	public void draw(Graphics2D g) {
		g.setColor(Color.GREEN);
		g.draw(this.rect);

	}

	@Override
	public double getWidth() {
		return rect.getWidth();
	}

	@Override
	public double getHeight() {
		return rect.getHeight();
	}

}
